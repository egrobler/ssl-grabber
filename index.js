const chalk       = require('chalk');
const clear       = require('clear');
const figlet      = require('figlet');
const files = require('./lib/files');
const inquirer  = require('./lib/inquirer');
const https= require('https');
const http= require('http');
const readline = require('readline');
const fs = require('fs');
const uuidv4 = require('uuid/v4');
const path = require('path');

clear();
console.log(
  chalk.yellow(
    figlet.textSync('URL Parser', { horizontalLayout: 'full' })
  )
);

const run = async () => {
    const question = await inquirer.askFileNames();
    const numlines = await files.getNumLines(question.filename);
    const output_file = question.output;

    const rl = readline.createInterface({
        input: fs.createReadStream(question.filename),
        crlfDelay: Infinity
    });

    function sleep (time) {
      return new Promise((resolve) => setTimeout(resolve, time));
    }

    fs.unlink(output_file, (err) => {
      //if (err) throw err;
      console.log(`successfully deleted ${output_file}`);
    });

    let counter = 1;
    rl.on('line', (line) => {
        const options_https = {
          hostname: line,
          port: 443,
          path: '/',
          method: 'GET',
          rejectUnauthorized: false,
          timeout: 500,
          requestCert: false
        };
        const options_http = {
          hostname: line,
          port: 80,
          path: '/',
          method: 'GET',
          timeout: 500
        };
        https.get(options_https, (res) => {
            console.log(`${counter} :: HTTPS :: Line from file: ${line}`);
            counter++;

            sleep(500).then(() => {
                res.read();
                  res.resume();
                return;
            });
            fs.appendFileSync(output_file, `${((counter==2)?"":"\n")}${line},${res.headers.server}`, (err) => {
                if (err) throw err;
            });
        }).on('error', (e) => {
            http.get(options_http, (res) => {
                console.log(`${counter} :: HTTP  :: Line from file: ${line}`);
                counter++;
                sleep(500).then(() => {
                  res.read();
                  res.resume();
                  return;
              });
                fs.appendFileSync(output_file, `${((counter==2)?"":"\n")}${line},${res.headers.server}`, (err) => {
                    if (err) throw err;
                });
            }).on('error', (e) => {
                //console.error(e);
            });
            //console.error(e);
        });
    });


}

run();
