const fs = require('fs');
const path = require('path');
const exec = require('child_process').exec;

module.exports = {
  getCurrentDirectoryBase : () => {
    return path.basename(process.cwd());
  },

  directoryExists : (filePath) => {
    try {
      return fs.statSync(filePath).isDirectory();
    } catch (err) {
      return false;
    }
  },
  getNumLines : (file) => {
    try {
      return new Promise(resolve => {
        exec(`wc -l ${file}`, function (error, results) {
            resolve(results.trim().split(' ')[0]);
        });
      });
    } catch (err) {
      console.log(err);
      return false;
    }
  }
};