const inquirer   = require('inquirer');
const files      = require('./files');

module.exports = {
  askFileNames: () => {
    const questions = [
      {
        name: 'filename',
        type: 'input',
        message: 'Enter the filename to parse:',
        default: 'short1.txt'
      },
      {
        name: 'output',
        type: 'input',
        message: 'Enter the CSV filename (output):',
        default: 'output.csv'
      }
    ];
    return inquirer.prompt(questions);
  },
  askGithubCredentials: () => {
    const questions = [
      {
        name: 'username',
        type: 'input',
        message: 'Enter your GitHub username or e-mail address:',
        validate: function( value ) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter your username or e-mail address.';
          }
        }
      },
      {
        name: 'password',
        type: 'password',
        message: 'Enter your password:',
        validate: function(value) {
          if (value.length) {
            return true;
          } else {
            return 'Please enter your password.';
          }
        }
      }
    ];
    return inquirer.prompt(questions);
  },
}