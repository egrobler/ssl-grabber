const chalk       = require('chalk');
const clear       = require('clear');
const figlet      = require('figlet');
const files = require('./lib/files');
const inquirer  = require('./lib/inquirer');
const readline = require('readline');
const fs = require('fs');
const uuidv4 = require('uuid/v4');
const path = require('path');
const { exec } = require('child_process');

clear();
console.log(
  chalk.yellow(
    figlet.textSync('URL Parser', { horizontalLayout: 'full' })
  )
);

const run = async () => {
    const question = await inquirer.askFileNames();
    const numlines = await files.getNumLines(question.filename);
    const output_file = question.output;

    const rl = readline.createInterface({
        input: fs.createReadStream(question.filename),
        crlfDelay: Infinity
    });

    fs.unlink(output_file, (err) => {
      //if (err) throw err;
      console.log(`successfully deleted ${output_file}`);
    });

    let counter = 1;
    rl.on('line', async (line) => {
      await exec(`curl -I --resolve -k --insecure --max-time 05 'https://${line}'`, (error, stdout, stderr) => {
        if (error) {
          //console.error(`exec error: ${error}`);
          //throw error;
        }
        //Work on the output..
        let str_to_write = "";
        let resultArr = stdout.split('\n').filter(row => row.indexOf('Server') == 0);
        if (resultArr.length > 0) {
          let serverInfo = resultArr[0].split(':')[1].trim();
          str_to_write = `${line},${serverInfo}`;
          console.log(`${line} :: ${serverInfo}`);
        } else {
          str_to_write = `${line},undefined`;
          console.log(`${line} :: undefined`);
        }


        fs.appendFileSync(output_file, `${((counter==2)?"":"\n")}${str_to_write}`, (err) => {
                if (err) {console.log(`fs Error: ${err}`);throw err;};
            });
      });

    });


}

run();
